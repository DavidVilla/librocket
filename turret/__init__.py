# -*- mode:python; coding:utf-8; tab-width:4 -*-

import time
from rocket_backend import RocketManager

DOWN = 0
UP = 1
LEFT = 2
RIGHT = 3
FIRE = 4
STOP = 5
LASER = 6

str_commands = ['down', 'up', 'left', 'right', 'fire']


class Turret(object):
    def __init__(self):
        self.manager = RocketManager()
        self.manager.acquire_devices()

        self.launcher = self.manager.launchers[0]
#        print "limits:", self.launcher.check_limits()
#        print "laser:", self.launcher.has_laser

    def _is_end(self, direction):
#        print "end ->", str_commands[direction], self.launcher.previous_limit_switch_states[direction]
        return self.launcher.check_limits()[direction]

    def _checked_move(self, direction):
        if self._is_end(direction):
            return

        self.launcher.start_movement(direction)

    def down(self):
        self._checked_move(DOWN)

    def up(self):
        self._checked_move(UP)

    def left(self):
        self._checked_move(LEFT)

    def right(self):
        self._checked_move(RIGHT)

    def stop(self):
        self.launcher.stop_movement()
        self.launcher.check_limits()

    def fire(self):
        self.launcher.start_movement(FIRE)
